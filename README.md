#       HA DEPLOY

Home of the code for automated HA Deployments.  For a quick refresher on Iguana
High Availability concepts see the quick notes below or read the document "HA
Iguana Comprehensive Technical Overview" in Google Drive for a very detailed
overview.

The code is provided in a git repository with a branch called lb-init for
initializing the HAProxy instances, and branch called wk-init for initializing
the instances on which Iguana will run. It's a strange way to set things up,
but ... (this needs to be updated after branches are merged).

Setup your project like so

    +-- HA-Deploy
    |   +-- code
    |   |   +-- ha-deploy
    |   |   +-- ha-deploy-release
    |   +-- notes
    |   |   +-- whaterver you want

##      PLATFORMS

These scripts are different depending on whether VMware, AWS, or no IaaS
platform (metal) is being used.  So for wk-init and lb-init, there are three
ways to call them.  Currently they are separate scripts, but we may merge them
into one and call with different parameters.  This way there are many scripts
but they are each simple - another approach is to have one complex script.

    +-- lb-init
    |   +-- lb-init-metal
    |   +-- lb-init-aws
    |   +-- lb-init-vmware
    +--wk-init
    |   +-- wk-init-metal
    |   +-- wk-init-aws
    |   +-- wk-init-vmware


###      Metal

This is when running on no IaaS platform.  All system configuration is done
with the standard Linux utilities for the distribution (CentOS in this case).
Currently only metal is completed.

###      IaaS

Infrastructure as a Server platforms such as VMware and AWS have API for
manipulating instances as well as network configuration such as firewalls.
With VMware, you can usually use the standard utilities while on the instance
but can manage an instance remotely with APIs.  For AWS you must use their
APIs.

Writing these scripts will be a matter of copying the metal script and finding
the configuration steps which must be made with the platform APIs.

#       USING HA-DEPLOY SCRIPTS

In addition to the ha-deploy repository and its branches, there is also an
ha-deploy-release repository.  A very simple makefile copies the required files
and archives them into the release repository.  This makes it very simple to
download and run the scripts in an automated manner or from a remote site.

When running the metal scripts, the four HA instance will already exist.  Log
into them and use curl to download the tar file containing the release repo.
Then edit the configuration files, and then run the {wk,lb}-bootstrap script.

When running the VMware and AWS scripts, you will be logged into a workstation
with SSH access to the network HA will be installed on.  The controller script
will create the instances, then build the correct configuration, upload the
configuration and the scripts to the instance and then run them (ideally).

#       DEVELOPING HA DEPLOY SCRIPTS

Create an project environment by creating a top-level folder called HA-Deploy
(for example) and under that, a code folder and any other folders that you use
to organize projects (notes, meeting minutes, etc.).

In the code folder, clone the ha-deploy project and the ha-deploy-release
projects. The makefile in ha-deploy, if properly placed beside the
ha-deploy-release project, should not need to be edited since it uses a
relative path and expects the release repo nexts to it.

You are now free to update the code by create feature branches. You can use
forked-repo style workflow if you prefer.

---

#       IGUANA HIGH AVAILABILITY QUICK NOTES

Iguana is not a single application or service.  Each LLP/HTTP inbound is
another feed that must be managed by the HA software.  Each From Translator and
From File is also generating data, which is then processed.

HAProxy is used so that Iguana can run on Windows or Linux.  Keepalived manages
the VIP (Iguana's production IP) which can be assigned to either HAProxy
instance.  Feeds come though the VIP to HAProxy and get passed on the Iguana.

The Poller Manager channel manages From Translator and From Files.  Only one
can run at a time, or else you could have issues with both trying to access the
data sources at the same time.  Polling channels run on set intervals.  Even
once a second is extremely slow compared to what load balancers usually handle.
Keep things simple and only ever run one at a time.

#       IGUANA HA CONFIGURATIONS

##      Instant Failover

The default and simplest configuration.  Always use this, and don't even talk
about other alternatives unless the client brings it up.  All behaviour is
Active/Passive.  All traffic goes to, and all pollers run on, the primary.
When the primary fails all traffic is redirected to, and all pollers start on,
the secondary.  Only two Iguana instances are needed, one on each server.

THAT'S IT!!!

##      Queue Recovery

For this three Iguana instances are needed, two on each machine.  The instances
must have production quality shared storage for their working directories.

Instance one has WK1 and instance two has WK2 and WK1qr (the qr stands for
'queue recoverer').  WK1 runs under normal conditions.  When there is a failure
WK2 starts (WK2 is WK1's backup).  A channel running on WK2 called 'Instance
Manager' notices WK1 is down and starts WK1qr on instance two.  WK1qr start
from the exact same working directory on the shared storage, thus recovering
the queue.

Because WK2 will start accepting messages before WK1's queue is recovered, this
provides queue recovery but not sequencing.

##      Sequencing

To achieve this, there is a simple trick.  Split up the channels and run half
on WK1 and half on WK2.  The can be grouped by business function, department,
or randomly.  Both WK's accept traffic for their own channels, and there is
also a QR instance on each, but things are different logically.

WK2 is no longer WK1's backup.  It is its own instance.  WK1qr is WK1's backup
and WK2qr is WK2's backup.  You could name them WK1bak and WK2 bak.  See the
diagrams in Google Drive for clarification.

When WK1 fails, the same procedure as in Queue Recovery takes place for WK1's
channels.  WK2's channels continue to function, no downtime for those channels.
Instance Manager on WK2 starts WK1bak.  The queue is recovered for WK1 and
WK1bak keeps on processing new messages.

The Split Channels approach is the way to do Queue Recovery and Sequencing.  It
also has the benefit of keeping both servers busy, which some people care
about.

#       ACTIVE/ACTIVE vs. ACTIVE/PASSIVE

People how talk about HA love talking about this.  Remember the following
distilled rules when considering this subject in regards to Iguana.

1. A/A works fine with From LLP and From HTTP, but persistent connections cause
   strange behaviour.  A connection will stick to where it's sent but then go
   to the next whenever a condition causes a disconnect.  You'll always need to
   look to see which WK is currently accepting messages.
2. A/A works fine with Queue Recovery, but is incompatible with sequencing.
3. A/A does not make sense for polling channels.


