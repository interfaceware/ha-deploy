#!/usr/bin/bash

# --------------------------------------------------------------------------- #
# -- HOW TO USE THIS COMMAND ------------------------------------------------ #
# --------------------------------------------------------------------------- #
#
# ** WHEN SIMPLE FAILOVER IS CONFIGURED **
# There is ONE way to run this command for each server:
# 1) From the First Server:
#    A. `iguana-admin start wk1`
#      a. Uses the command  -> /${IGUANAS}/wk1/wk1
#      b. Does not rename any license
# 2) From the Second Server:
#    A. `iguana-admin start wk2`
#      a. Uses the command /${IGUANAS}/wk2/wk2
#      b. Does not rename any license
#
# The absence of THE_LICENSE parameter ($3) indicates to the script that
# license renaming is NOT needed.
#
# ** WHEN QUEUE RECOVERY IS CONFIGURED **
# There are FOUR licenses:
#  1) /${IGUANAS}/wk1/IguanaLicense-primary <- wk1 Primary Iguana on First  Server
#  2) /${IGUANAS}/wk1/IguanaLicense-backup  <- wk2 Backup  Iguana on First  Server
#  4) /${IGUANAS}/wk2/IguanaLicense-primary <- wk2 Primary Iguana on Second Server
#  3) /${IGUANAS}/wk2/IguanaLicense-backup  <- wk1 Backup  Iguana on Second Server
#
# There are TWO ways to run this command from each server:
# 1) From the First Server:
#   A. `iguana-admin start wk1 primary`
#     a. Uses the license   -> /${IGUANAS}/wk1/IguanaLicense-primary
#     b. Used the command   -> /${IGUANAS}/wk1/wk1
#   B. `iguana-admin start wk2 secondary`
#     a. Uses the license   -> /${IGUANAS}/wk2/IguanaLicense-secondary
#     b. Used the command   -> /${IGUANAS}/wk2/wk2
# 2) From the Second Server:
#   A. `iguana-admin start wk1 secondary`
#     a. Uses the license   -> /${IGUANAS}/wk1/IguanaLicense-secondary
#     b. Used the command   -> /${IGUANAS}/wk1/wk1
#   B. `iguana-admin start wk2 primary`
#     a. Uses the license   -> /${IGUANAS}/wk2/IguanaLicense-primary
#     b. Used the command   -> /${IGUANAS}/wk2/wk2
#
# The absence of THE_LICENSE parameter ($3) indicates to the script that
# license renaming is NOT needed.
#
# You must remember that wk1 is the primary on First Server and that wk2 is the
# primary on Second Server.
#
# The main purpose of this script is the rename the license appropriately. If
# you run `iguana-admin start wk2 secondary` from Second Server you will get
# the wrong license.

MOUNTPOINT="%s"
IGUANAS="%s"
LOG_FILE="%s"
THE_ACTION=$1
THE_IGUANA=$2
THE_LICENSE=$3
THE_DATE=`date`
THE_IP=`hostname -i | cut -d ' ' -f2`
IGUANA_PATH="${IGUANAS}/${THE_IGUANA}"
IGUANA_BINARY="${IGUANA_PATH}/${THE_IGUANA}"
INSTALLED_LICENSE="${IGUANA_PATH}/IguanaLicense"
LICENSE="${INSTALLED_LICENSE}-${THE_LICENSE}"
PID_FILE="${IGUANA_PATH}/${THE_IGUANA}.pid"

if [ "$DEBUG" ]; then
   echo "THE_ACTION=${THE_ACTION}"
   echo "THE_IGUANA=${THE_IGUANA}"
   echo "THE_LICENSE=${THE_LICENSE} (Not used for instant failover)"
   echo "THE_DATE=${THE_DATE}"
   echo "THE_IP=${THE_IP}"
   echo "IGUANA_PATH=${IGUANA_PATH}"
   echo "IGUANA_BINARY=${IGUANA_BINARY}"
   echo "INSTALLED_LICENSE=${INSTALLED_LICENSE} (Not used for instant failover)"
   echo "LICENSE=${LICENSE} (Not used for instant failover)"
   echo "PID_FILE=${PID_FILE}"
   exit 0
fi

## HELPFUL FUNCTIONS ##

function log-it {
    echo $1 | tee -a $LOG_FILE
}

function check-result {
    if [ "$?" != 0 ] ; then
        log-it " - $1"
        exit 1
    fi
}

function cleanup-after-crash {
    log-it "  - Checking for old fifo's to remove"
    for x in /tmp/IFWSVC_*; do
      # Make sure its a fifo/pipe before continuing. Globs which match nothing
      # are taken as literal strings.
      [ -p "$x" ] || continue
        fuser -s $x;
        # $? != 0 means it's an invalid pipe.
        if [ "$?" != 0 ]; then
            log-it "  - Deleting old fifo $x"
            rm $x
        fi;
    done
}

function rename-license {
    log-it "  - Renaming $LICENSE to $INSTALLED_LICENSE"
    cp $LICENSE $INSTALLED_LICENSE
    check-result "Could not rename the license file"
}

function aws-precheck {
    log-it " - AWS PRECHECK"
    if [ -b /dev/xvdi ] ; then
        log-it "  - /dev/xvdi is present"
    else
        log-it "  - ERROR: /dev/xvdi (Iguana) is not attached to the instance"
        exit 1
    fi

    if grep -q $MOUNTPOINT /etc/mtab; then
        log-it "  - $MOUNTPOINT is mounted"
    else
        log-it "  - Mounting $MOUNTPOINT"
        mount $MOUNTPOINT
        check-result "ERROR: Failed to mount $MOUNTPOINT"
    fi
}

## END OF FUNCTIONS ##

log-it "[$THE_DATE - $THE_IP]:"

# aws-precheck

if [ -d "$IGUANAS" ]; then
    log-it " - $IGUANAS directory exists"
else
    log-it "$IGUANAS directory does not exist"
    exit 1
fi

# Iguana creates the pidfile in whichever directory it's in.
# Make sure the PID files are always in the same place.
cd $IGUANA_PATH


case "$THE_ACTION" in
"start")
    cleanup-after-crash
    if [ "$THE_LICENSE" ]; then
       rename-license
    fi
    log-it "  - Starting ${THE_IGUANA} with command $IGUANA_BINARY"
    $IGUANA_BINARY
    exit 0
    ;;
"stop")
    log-it "  - Stopping $THE_IGUANA iguana (PID `cat $PID_FILE`)"
    kill -TERM `cat $PID_FILE`
    exit 0
    ;;
*)
    log-it "usage: iguana-admin (start|stop) <iguana-name> (primary|backup)"
    exit 4
    ;;
esac
