
#!/usr/bin/env python
from __future__ import print_function
import argparse
import os
import sys
import urllib2
import tarfile
import subprocess
import time
import shutil
import json
import requests


# Which OS does python detect?
POSIX = os.name == "posix"
WINDOWS = os.name == "nt"

CONFIG = {}
config_file = "../wk-config.json"
if not os.path.isfile(config_file):
    print("ERROR! config.json does not exist. It should be at the top-level!")
    sys.exit(1)

with open(config_file) as c:
    CONFIG = json.load(c)

OS = CONFIG["os"].lower()
ARCH = CONFIG["arch"].lower()
NIC = CONFIG["network_interface"].lower()
IPADDR = CONFIG["ip_address"]
GATEWAY = CONFIG["gateway"]
DNS = CONFIG["dns"]
HOSTNAME = CONFIG["hostname"]
VERSION = CONFIG["iguana_version"].replace(".", "_")
IGUANA_DASHBOARD_PORT = CONFIG["iguana_dashboard_port"]
IGUANA_HTTPS_PORT = CONFIG["iguana_https_port"]
IGUANA_LLP_PORT_RANGE = CONFIG["iguana_llp_port_range"]
 
# Templates for system files and scripts.
IGUANA_SERVICE_FILE = CONFIG["templates"]["service_file"]
SYSTEMD_UNIT_FILE = CONFIG["templates"]["systemd_file"]
IGUANA_ADMIN_SCRIPT = CONFIG["templates"]["iguana_admin_file"]

# Location on the worker where ALL Iguanas are installed and versioned.
ROOTDIR = CONFIG["root_dir"]

# Directory for storing downloaded tarfiles.
TARDIR = CONFIG["tar_dir"]

if not os.path.isdir(TARDIR):
    os.makedirs(TARDIR)

# Log file for iguana-admin script
IGUANA_LOG_FILE_BASENAME = CONFIG["iguana_log_file"]

# Paths to system files.
SYSTEMD_PATH = CONFIG["systemd_path"]
BIN_PATH = CONFIG["bin_path"]

TARFILE_BASENAME = ""

# TARDIR + TARFILE_BASENAME
TARFILE_TO = ""

# URL or path to the tarfile to use
INSTALL_FROM = ""

# ROOTDIR + Iguana name for installation
INSTALL_TO = ""

# ROOTDIR + Iguana name for uninstallation
REMOVE_FROM = ""

HA_TOOLS_REPO_URL = CONFIG["ha_tools_repo_url"]
HA_TOOLS_REPO_NAME = CONFIG["ha_tools_repo_name"]
HA_TOOLS_REPO_USERNAME = CONFIG["ha_tools_repo_username"]
HA_TOOLS_REPO_PASSWORD = CONFIG["ha_tools_repo_password"]
HA_TOOLS_TO_IMPORT = CONFIG["ha_tools_to_import"]


def build_url():
    """Build the URL from which to fetch Iguana if needed. Examples:
    http://dl.interfaceware.com/iguana/linux/6_0_7/iguana_6_0_7_linux_ubuntu1404_x64.tar.gz
    http://dl.interfaceware.com/iguana/linux/6_0_7/iguana_6_0_7_linux_centos7_x64.tar.gz
    http://dl.interfaceware.com/iguana/windows/6_0_7/iguana_6_0_7_windows_x64.zip"""

    base = "http://dl.interfaceware.com/iguana"
 
    if OS == "windows":
        platform = "windows"
        ext = "zip"
        url_tpl =  "{b}/{p}/{v}/iguana_{v}_{p}_{a}.{e}"
    else:
        platform = "linux"
        ext = "tar.gz"
        url_tpl =  "{b}/{p}/{v}/iguana_{v}_{p}_{o}_{a}.{e}"

    url = url_tpl.format(b=base, p=platform, v=VERSION, o=OS, a=ARCH, e=ext)

    return url


def setup_install_iguana_paramters():
    global args, IGUANA_LOG_FILE

    parser = argparse.ArgumentParser()
    parser.add_argument("install-iguana")
    parser.add_argument("-n", "--name", help="Iguana instance/process name", required=True)
    parser.add_argument("-w", "--working-dir", help="Working directory path")
    parser.add_argument("-d", "--description", help="Service description")
    parser.add_argument("-D", "--display-name", help="Service display name")
    parser.add_argument("-f", "--force-download", help="Force re-download of tar file", action="store_true")
    args = parser.parse_args()
    IGUANA_LOG_FILE = IGUANA_LOG_FILE_BASENAME.replace(".", "-" + args.name + ".")


def print_installation_arguments():
    global INSTALL_FROM, INSTALL_TO, TARFILE_BASENAME, TARFILE_TO

    # Some tweaks to inputs.
    INSTALL_FROM = build_url()
    INSTALL_TO = '%s/%s' % (ROOTDIR, args.name)
    TARFILE_BASENAME = os.path.basename(INSTALL_FROM)
    TARFILE_TO = TARDIR + '/' + TARFILE_BASENAME
    
    print(" OS: %s" % OS)
    print(" ARCH: %s" % ARCH)
    print(" VERSION: %s" % VERSION)
    print(" ROOTDIR: %s" % ROOTDIR)
    print(" TARDIR: %s" % TARDIR)
    print(" BIN_PATH: %s" % BIN_PATH)
    print(" SYSTEMD_PATH: %s" % SYSTEMD_PATH)
    print(" IGUANA_LOG_FILE: %s" % IGUANA_LOG_FILE)
    print(" INSTALL_FROM: %s" % INSTALL_FROM)
    print(" INSTALL_TO: %s" % INSTALL_TO)
    print(" TARFILE_TO: %s" % TARFILE_TO)
    print("=-----------------------------------------------------------------=")
    print(" Begin installation of Iguana %s." % args.name)


def setup_configure_system_paramters():
    global args, IGUANA_LOG_FILE
    parser = argparse.ArgumentParser()
    parser.add_argument("configure-system")
    parser.add_argument("-n", "--name", help="Iguana instance/process name", required=True)
    args = parser.parse_args()
    IGUANA_LOG_FILE = IGUANA_LOG_FILE_BASENAME.replace(".", "-" + args.name + ".")


def setup_uninstall_iguana_parameters():
    global args, REMOVE_FROM, IGUANA_LOG_FILE
    parser = argparse.ArgumentParser()
    parser.add_argument("uninstall")
    parser.add_argument("-n", "--name", help="Iguana instance/process name", required=True)
    args = parser.parse_args()

    REMOVE_FROM = "%s/%s" % (ROOTDIR, args.name)
    IGUANA_LOG_FILE = IGUANA_LOG_FILE_BASENAME.replace(".", "-" + args.name + ".")
    

def setup_configure_iguana_parameters():
    global args, IGUANA_DASHBOARD_PORT, HA_TOOLS_REPO_URL
    parser = argparse.ArgumentParser()
    parser.add_argument("configure-iguana")
    parser.add_argument("-n", "--name", help="Iguana instance/process name")
    parser.add_argument("-p", "--port", help="Port the Iguana server will run on")
    parser.add_argument("-r", "--ha-tools", help="HARepository from which to pull in channels")
    args = parser.parse_args()


def setup_configure_haproxy_parameters():
    global args
    parser = argparse.ArgumentParser()
    parser.add_argument("configure-haproxy")
    parser.add_argument("-n", "--name", help="Iguana instance/process name")
    args = parser.parse_args()


def download_if_required():
    file_exists = os.path.isfile(TARFILE_TO)

    if INSTALL_FROM.startswith("http"):
        if file_exists and not args.force_download:
            print(" %s exists" % TARFILE_TO)
            print(" no need to download it again")
        else:
           do_download(INSTALL_FROM, TARFILE_BASENAME, TARFILE_TO)
    elif not file_exists:
        print(" Tar file does not exist")
        sys.exit(1)
    elif file_exists:
        print(" Found the tar file on disk")


def do_download(url, filename, filepath):
    request = urllib2.Request(url)
    connection = urllib2.urlopen(request)

    print(" Starting download of %s:" % filename)
    print("   downloading ...")

    # Overwrite if file exists is intentional.
    with open(filepath, "w") as TARFILE_TO:
        for the_bytes in connection:
            TARFILE_TO.write(the_bytes)
        TARFILE_TO.close()

    print(" Download completed, wrote to %s." % filepath)


def unpack_iguana_tar():
    print(" Setting up Iguana %s" % args.name)
    unpacked = ROOTDIR + "/iNTERFACEWARE-Iguana"
    unpacked_renamed = "%s/%s" % (ROOTDIR, args.name)

    if os.path.isdir(unpacked_renamed):
        print(" %s already exists. Delete it and run again" % unpacked_renamed)
        sys.exit(1)

    print(" Unpacking tar to %s ..." % ROOTDIR)
    tar = tarfile.open(TARFILE_TO)
    tar.extractall(path=ROOTDIR)
    tar.close()

    print(" Renaming %s to %s." % (unpacked, unpacked_renamed))
    os.rename(unpacked, unpacked_renamed)

    print(" Finished unpacking Iguana")


def update_port_posix():
    """OS-specific method of updating"""
    print(" Setting Iguana port to %s" % IGUANA_DASHBOARD_PORT)
    subprocess.call(["sed", "-i.default-port", 's/port="6543"/port="%s\"/' % IGUANA_DASHBOARD_PORT,
        "%s/%s/IguanaConfigurationRepo/IguanaConfiguration.xml" % (ROOTDIR, args.name)])


def update_port_windows():
    pass


def update_port():
    if POSIX: update_port_posix()
    else:     update_port_windows()


def post_to_iguana(url, params, max_retries=5, username="admin", password="password"):
    tries = 1
    while True:
        print("   Attempt %s of %s" % (tries, max_retries))
        if tries > max_retries:
            print(" All out of tries. Abort!")
            sys.exit(1)
        try:
            print(" POSTing to %s" % url)
            response = requests.post(url, data=params, auth=(username, password))
            print(" Received response %s" % response.status_code)
            
            if response.status_code == 200 or tries > max_retries:
                return response
            else:
                tries += 1
                time.sleep(10)
        except Exception as e:
            print(" Exception while making request: %s" % e)
            tries += 1
            time.sleep(10)


def add_ha_tools_repo():
    params = {
        "repo_name": HA_TOOLS_REPO_NAME,
        "original_repo_name": "",
        "repo_protocol": "https",
        "original_repo_protocol": "",
        "local_path": "",
        "http_path": "http://",
        "https_path": HA_TOOLS_REPO_URL,
        "ssh_path": "ssh://",
        "repo_key_path": "",
        "action": "add"
        }

    url = "http://localhost:%s/repositories/do_add_or_edit" % IGUANA_DASHBOARD_PORT
    response = post_to_iguana(url, params)


def import_ha_tools():
    params = {
        "remote_name": HA_TOOLS_REPO_NAME,
        "auth_user": HA_TOOLS_REPO_USERNAME,
        "auth_password": HA_TOOLS_REPO_PASSWORD
        }

    url = "http://localhost:%s/sc/view_remote_channels" % IGUANA_DASHBOARD_PORT

    retries = 3

    print(" Fetching channel_list from repository")
    response = post_to_iguana(url, params)

    content = json.loads(response.content)
    version_key = "6_1_0"
    channels = content["channel_list"][version_key]
    channels_to_import = []

    for channel in channels:
# If you receive an error when indexing "name" then it's probably because
# the version_key needs to be updated (or determined dynamically). 
        name = channel["name"]
        if name in HA_TOOLS_TO_IMPORT:
            print(' Importing "%s"' % name)
            channel["selected"] = True;
            channels_to_import.append(channel)
        else:
            print(' HA Tool "%s" is not configured for import' % name)

    params = {
        "sc_task": "import_channels",
        "auth_user": HA_TOOLS_REPO_USERNAME,
        "auth_password": HA_TOOLS_REPO_PASSWORD,
        "commit_message": "Automated pull of HA Tools",
        "channels_version": version_key,
        "remote_name": HA_TOOLS_REPO_NAME,
        "channel_import_list": json.dumps(channels_to_import)
        }
 
    url = "http://localhost:%s/import_channels" % IGUANA_DASHBOARD_PORT

    print(" Fetching channels from repository")
    response = post_to_iguana(url, params)


def add_ha_tools():
    print("   Adding the HA Tools repository")
    add_ha_tools_repo()
    print("   Importing the HA Tools")
    import_ha_tools()


def install_iguana_service():
    display_name = args.display_name or "Iguana instance installed by a script."
    service_name = args.name
    description = args.description or "Iguana instance installed by a script"
    command = "%s/iguana" % INSTALL_TO

    if args.working_dir:
        command += " --working_dir %s" % args.working_dir

    print(" Generating new Iguana service configuration for %s" % args.name)

    with open(IGUANA_SERVICE_FILE) as tpl:
        iguana_service_file_tpl = tpl.read()

    new_service_config = iguana_service_file_tpl % (display_name, service_name, description, command)

    print(" Rewriting Iguana service configuration file (.hdf) for %s" % args.name)
    old_hdf = open("%s/iguana_service.hdf" % INSTALL_TO, "w")
    old_hdf.write(new_service_config)
    old_hdf.close()

    print(" Renaming iguana_service.hdf to %s.hdf" % args.name)
    os.rename("%s/iguana_service.hdf" % INSTALL_TO, "%s/%s.hdf" % (INSTALL_TO, args.name))

    iguana_service_name = "%s/%s" % (INSTALL_TO, args.name)
    print(" Renaming iguana_service to %s" % args.name)
    os.rename("%s/iguana_service" % INSTALL_TO, iguana_service_name)

    print(" Starting Iguana so it will check out its configuration")

    the_binary = "%s/%s/iguana" % (ROOTDIR, args.name)
    print(" Starting Iguana with %s" % the_binary)

    devnull = open(os.devnull, "w")
    iguana_process = subprocess.Popen([the_binary, "--run"], stdout=devnull, stderr=subprocess.STDOUT)
    print(" Wait 5 seconds for Iguana to checkout it configuration")
    time.sleep(5)
    iguana_process.kill()
    devnull.close()
    print(" Iguana is installed (though not configured)")


def install_systemd_service():
    """Install systemd unit file to /etc/systemd/system, reload systemd daemon"""

    real_user = os.environ.get("SUDO_USER")

    with open(SYSTEMD_UNIT_FILE) as tpl:
        systemd_tpl = tpl.read()

    systemd_file_name = "%s/%s.service" % (SYSTEMD_PATH, args.name)
    print(" Creating %s" % systemd_file_name)
    with open(systemd_file_name, "w") as systemd_file:
        systemd_file.write(systemd_tpl.format(
            desc = "Automated installation of Iguana " + args.name,
            name = args.name,
            user = real_user))

    if not os.path.isfile(systemd_file_name):
        print("Could not create the system file. Exiting")
        sys.exit(4)

    # Load the new service file, allowing "systemctl start args.name" to start
    # the service.
    print(" Reloading systemctl")
    subprocess.call(["systemctl", "daemon-reload"])
    print(" Setting the %s service to start on system startup" % args.name)
    subprocess.call(["systemctl", "enable", "--quiet", args.name])


def install_iguana_log_file():
    if os.path.isfile(IGUANA_LOG_FILE):
        print(" Log file %s exists" % IGUANA_LOG_FILE)
    else:
        print(" Creating log file %s" % IGUANA_LOG_FILE)
        open(IGUANA_LOG_FILE, "w").close()

# TODO Linux specific
    real_user_id = int(os.environ.get("SUDO_UID"))
    real_group_id =  int(os.environ.get("SUDO_GID"))
    print(" Chown log file to %s" % real_user_id)
    os.chown(IGUANA_LOG_FILE, real_user_id, real_group_id)


def install_iguana_admin_script():
    """Install iguana-admin script to /usr/local/bin, make it executable. Also
    create its log file."""

    with open(IGUANA_ADMIN_SCRIPT) as tpl:
        iguana_admin_script_tpl = tpl.read()

    iguana_admin_script_name = "%s/iguana-admin-%s" % (BIN_PATH, args.name)
    print(" Creating %s" % iguana_admin_script_name)
    with open(iguana_admin_script_name, "w") as iguana_admin_script:
        iguana_admin_script.write(iguana_admin_script_tpl % (ROOTDIR, ROOTDIR, IGUANA_LOG_FILE))
    print(" Making admin script executable")
    os.chmod(iguana_admin_script_name, 0755)


def deregister_service():
    pass 


def remove_iguana_directory(appdir):
    if not os.path.isdir(appdir):
        print(" Iguana %s does not exist." % appdir)
        return

    print(" Deleting %s" % appdir)
    shutil.rmtree(appdir)

    if os.path.isdir(appdir):
        print(" WARNING! %s was not removed" % appdir)
        return False
    else:
        print(" %s was removed" % appdir)
        return True
    

def remove_unit_file(unit_file_name):
    if not os.path.isfile(unit_file_name):
        print(" Unit file %s does not exist." % unit_file_name)
        return

    print(" Deleting %s" % unit_file_name)
    os.remove(unit_file_name)

    if os.path.isfile(unit_file_name):
        print(" WARNING! %s was not removed" % unit_file_name)
        return False
    else:
        print(" %s was removed" % unit_file_name)
        print(" Reloading systemctl")
        subprocess.call(["systemctl", "daemon-reload"])
        return True


def remove_admin_script(iguana_admin_script_name):
    if not os.path.isfile(iguana_admin_script_name):
        print(" Admin script %s does not exist." % iguana_admin_script_name)
        return False

    print(" Deleting %s" % iguana_admin_script_name)
    os.remove(iguana_admin_script_name)

    if os.path.isfile(iguana_admin_script_name):
        print(" %s was not removed" % iguana_admin_script_name)
        return False
    else:
        print(" %s was removed" % iguana_admin_script_name)
        return True


def start_iguana_service():
    if WINDOWS:
        sys.exit("start_iguana_service: not implemented for Windows")
    elif POSIX:
        subprocess.call(["sudo", "systemctl", "start", args.name])
        time.sleep(5)


def stop_iguana_service():
    if WINDOWS:
        sys.exit("stop_iguana_service: not implemented for Windows")
    elif POSIX:
        print(" Stopping Iguana service")
        subprocess.call(["systemctl", "stop", args.name])

def basic_system_setup():

    print(" Setting the hostname")
    cmd_str = "sudo hostnamectl set-hostname {}".format(HOSTNAME)
    p = subprocess.Popen(cmd_str.split())
    p.wait()

    print(" Setting up static networking configuration.")
    cmd_str = 'sudo nmcli connection modify {nic} ipv4.dns {dns_gw} '\
            + 'ipv4.gateway {dns_gw} ipv4.addresses {ip_addr} ipv4.method manual connection.autoconnect yes'
    formatted = cmd_str.format(nic=NIC, dns_gw=DNS, ip_addr=IPADDR)
    p = subprocess.Popen(formatted.split())
    p.wait()

    print(" Setting up the firewall")
    cmd_str = "sudo firewall-cmd --add-port={iguana_dash}/tcp --add-port={llp_range}/tcp --add-port={iguana_https}/tcp --permanent"
    formatted = cmd_str.format(iguana_dash=IGUANA_DASHBOARD_PORT, llp_range=IGUANA_LLP_PORT_RANGE, iguana_https=IGUANA_HTTPS_PORT)
    p = subprocess.Popen(formatted.split())
    p.wait()

def uninstall():
    setup_uninstall_iguana_parameters()

    print("=-----------------------------------------------------------------=")
    print(" UNINSTALLING IGUANA")
    print("=-----------------------------------------------------------------=")
    print(" This command must be run as sudo. If you edit this file to do     ")
    print(" anything anywhere other than in the users home directory or the   ")
    print(" Iguana installation directory then please BE CAREFUL!             ")
    print("=-----------------------------------------------------------------=")

    if not os.geteuid() == 0:
        print(" ERROR: This command must be run as sudo!")
        print("=-----------------------------------------------------------------=")
        sys.exit(13)

    appdir = "%s/%s" % (ROOTDIR, args.name)
    unit_file_name = "/etc/systemd/system/%s.service" % args.name
    iguana_admin_script_name = "/usr/local/bin/iguana-admin-%s" % args.name

    print(" WARNING! WARNING!")
    print("=-----------------------------------------------------------------=")
    print(" You are about to destroy the following files/directories!         ")
    print("     %s" % appdir)
    print("     %s" % unit_file_name)
    print("     %s" % iguana_admin_script_name)
    print("=-----------------------------------------------------------------=")
    proceed = raw_input(" Are there paths correct? Are you sure? (y/n): ")
    print("=-----------------------------------------------------------------=")

    if proceed == "y":
        print(" Stopping Iguana " + args.name)
        stop_iguana_service()
        remove_iguana_directory(appdir)
        remove_unit_file(unit_file_name)
        remove_admin_script(iguana_admin_script_name)
        print("=-----------------------------------------------------------------=")
        print(" The log file %s has not been removed" % IGUANA_LOG_FILE)
        print(" Iguana %s has been removed" % args.name)
        print("=-----------------------------------------------------------------=")
        sys.exit(0)
    else:
        print(" ABORT! You did not type 'y'")
        print("=-----------------------------------------------------------------=")
        sys.exit(1)


def install_iguana():
    if os.geteuid() == 0:
        print(" ERROR: This command must NOT be run with sudo")
        sys.exit(13)

    setup_install_iguana_paramters()

    print("=-----------------------------------------------------------------=")
    print(" INSTALLING AND CONFIGURING IGUANA                                 ")
    print("=-----------------------------------------------------------------=")
    print_installation_arguments()
    print("=-----------------------------------------------------------------=")
    print("= FETCHING IGUANA                                                 =")
    print("=-----------------------------------------------------------------=")
    download_if_required()

    print("=-----------------------------------------------------------------=")
    print("= UNPACKING IGUANA                                                =")
    print("=-----------------------------------------------------------------=")
    unpack_iguana_tar()

    print("=-----------------------------------------------------------------=")
    print("= CONFIGURING IGUANA                                              =")
    print("=-----------------------------------------------------------------=")
    install_iguana_service()

    print("=-----------------------------------------------------------------=")
    print(" Iguana is installed. Install admin script with the command:      =")
    print("      $ sudo ./autoinstall configure-system --name %s              " % args.name)
    print("=-----------------------------------------------------------------=")


def configure_system():
    if not os.geteuid() == 0:
        print(" ERROR: This command must be run with sudo")
        sys.exit(13)

    setup_configure_system_paramters()
    global IGUANA_LOG_FILE
    IGUANA_LOG_FILE = IGUANA_LOG_FILE_BASENAME.replace(".", "-" + args.name + ".")

    print("=-----------------------------------------------------------------=")
    print(" CONFIGURING THE SYSTEM                                            ")
    print("=-----------------------------------------------------------------=")
    print(" This command must be run with sudo. If you edit this file to do   ")
    print(" anything anywhere other than in the users home directory or the   ")
    print(" Iguana installation directory then please BE CAREFUL!             ")
    print("=-----------------------------------------------------------------=")
    print("= BASIC SYSTEM SETUP                                              =")
    print("=-----------------------------------------------------------------=")
    basic_system_setup()

    print("=-----------------------------------------------------------------=")
    print("= INSTALL ADMIN SCRIPT                                            =")
    print("=-----------------------------------------------------------------=")
    install_iguana_admin_script() 

    print("=-----------------------------------------------------------------=")
    print("= INSTALL LOG FILE                                                =")
    print("=-----------------------------------------------------------------=")
    install_iguana_log_file()
    print("=-----------------------------------------------------------------=")
    print("= INSTALL SYSTEMD SERVICE                                         =")
    print("=-----------------------------------------------------------------=")
    install_systemd_service()

    print("=-----------------------------------------------------------------=")
    print(" Finished! RUN THE FOLLOWING COMMANDS to ensure things are         ")
    print(" configured properly!                                              ")
    print("=-----------------------------------------------------------------=")
    print(" To confirm the iguana admin script is setup properly run:")
    print("     $ DEBUG=1 iguana-admin-{name} start {name}".format(name=args.name))
    print(" To confirm systemd service is loaded correctly run:")
    print("     $ systemctl status %s" % args.name)
    print(" To start the %s Iguana service run:" % args.name)
    print("     $ sudo systemctl start %s" % args.name)
    print(" To stop the %s Iguana service run:" % args.name)
    print("     $ sudo systemctl stop %s" % args.name)
    print("=-----------------------------------------------------------------=")


def configure_iguana():
    setup_configure_iguana_parameters()
    print("=-----------------------------------------------------------------=")
    print(" CONFIGURING IGUANA                                                ")
    print("=-----------------------------------------------------------------=")
    
    print(" Updating Iguana port")
    update_port()
    print(" Starting Iguana service ...")
    start_iguana_service()
    print(" Installing the HA Tools")
    add_ha_tools()
    

def configure_haproxy():
    setup_configure_haproxy_parameters()
    print("=-----------------------------------------------------------------=")
    print(" CONFIGURING HAPROXY                                               ")
    print("=-----------------------------------------------------------------=")
    
    print(" Installing HAProxy configuration")
    install_haproxy_configuration()
    print(" Starting Iguana service ...")
    start_iguana_service()
    print(" Installing the HA Tools")
    add_ha_tools()
    


def main():
    if len(sys.argv) == 1:
        print(" Interactive mode not implemented")
        sys.exit(0)
    
    if sys.argv[1] == "install-iguana":
        install_iguana()
        sys.exit(0)

    if sys.argv[1] == "configure-system":
        configure_system()
        sys.exit(0)

    if sys.argv[1] == "configure-iguana":
        configure_iguana()
        sys.exit(0)

    if sys.argv[1] == "uninstall-iguana":
        uninstall()
        sys.exit(0)
    
    if sys.argv[1] == "install-haproxy":
        configure_haproxy()

    print(" Usage: usage message goes here")
    sys.exit(1)


if __name__ == "__main__":
    main()
