#!/usr/bin/bash
#
# wk-bootstrap.bash
#
#    This is uploaded and ran on a new instance by HA Deploy Controller.  All
#    it does is unpack autoinstall (also uploaded by the Controller) and then
#    runs it.

LOGFILE="file.log"
INIT_PLATFORM=$1
WK_INIT_BASE="wk-init"
WK_INIT_SCRIPT="${WK_INIT_BASE}-${INIT_PLATFORM}.py"
WK_INIT_ARCHIVE="${WK_INIT_BASE}.tar.gz"
IGUANA_INSTANCE_NAME=$2

function log-it {
    echo $1 | tee -a $LOGFILE
}

# TODO: Proper usage message and parameter checking.
if [[ -z $INIT_PLATFORM ]]; then
    log-it "First parameter is empty. Should be 'metal', 'aws', or 'vmware'".
    exit 1
fi

if [[ -z $IGUANA_INSTANCE_NAME ]]; then
    log-it "Second parameter is empty. Should be an Iguana instance name (with no spaces)."
    exit 2
fi

log-it "Installing dependencies"
#sudo yum update -y
sudo yum install -y epel-release
sudo yum install -y python-pip
sudo pip --no-cache-dir install --upgrade pip
sudo pip --no-cache-dir install requests

if [[ ! -d $WK_INIT_BASE ]]; then
   log-it "About to un-archive $WK_INIT_ARCHIVE"
   tar xf $WK_INIT_ARCHIVE
fi

log-it "cd into $WK_INIT_BASE"
cd $WK_INIT_BASE
log-it "==================================================================="
log-it " CMD: python $WK_INIT_SCRIPT install-iguana --name $IGUANA_INSTANCE_NAME"
log-it "==================================================================="
python $WK_INIT_SCRIPT install-iguana --name $IGUANA_INSTANCE_NAME
wait
log-it " DONE INSTALLING IGUANA"

log-it "==================================================================="
log-it " CMD: sudo python $WK_INIT_SCRIPT configure-system --name $IGUANA_INSTANCE_NAME"
log-it "==================================================================="
sudo python $WK_INIT_SCRIPT configure-system --name $IGUANA_INSTANCE_NAME
wait
log-it " DONE CONFIGURING SYSTEM"

log-it "==================================================================="
log-it " CMD: python $WK_INIT_SCRIPT configure-iguana --name $IGUANA_INSTANCE_NAME"
log-it "==================================================================="
python $WK_INIT_SCRIPT configure-iguana --name $IGUANA_INSTANCE_NAME
wait
log-it " DONE CONFIGURING IGUANA"

sudo reboot
