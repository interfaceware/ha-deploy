#!/usr/bin/bash
#
# lb-bootstrap.bash
#
#    This is uploaded and ran on a new instance by HA Deploy Controller.  All
#    it does is unpack autoinstall (also uploaded by the Controller) and then
#    runs it.

LOGFILE="file.log"
INIT_PLATFORM=$1
LB_INIT_BASE="lb-init"
LB_INIT_SCRIPT="${LB_INIT_BASE}-${INIT_PLATFORM}.bash"
LB_INIT_ARCHIVE="${LB_INIT_BASE}.tar.gz"

function log-it {
    echo $1 | tee -a $LOGFILE
}

if [[ -z $INIT_PLATFORM ]]; then
    log-it "First parameter is empty. Should be 'metal', 'aws', or 'vmware'".
    exit 1
fi

log-it "About to un-archive $LB_INIT_ARCHIVE"
tar xf $LB_INIT_ARCHIVE
log-it "cd into $LB_INIT_BASE"
cd $LB_INIT_BASE
log-it "About to run $LB_INIT_SCRIPT"
sudo bash $LB_INIT_SCRIPT
