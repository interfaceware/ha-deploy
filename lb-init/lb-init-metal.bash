#!/usr/bin/bash
#
# lb-init-metal.bash
#   Run from a freshly installed CentOS 7 to configure an LB for IFW HA.  This
#   is intended to run on an instance which is not managed by Vmware or AWS.

CONFIG_FILE=../lb-config.bash
source $CONFIG_FILE

if [[ $DEBUG == 1 ]]; then
    cat $CONFIG_FILE
    exit 0
fi

# Set hostname
hostnamectl set-hostname $HOSTNAME

# Disable SELinux
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

# Set networking configuration
nmcli con mod $NIC ipv4.addresses $IPADDR
nmcli con mod $NIC ipv4.gateway $GATEWAY
nmcli con mod $NIC ipv4.dns "${DNS1} ${DNS2}"
nmcli con mod $NIC ipv4.method manual
nmcli con mod $NIC connection.autoconnect yes

# Allow HAProxy to bind to the VIP, even if it has not been assigned yet.
echo "net.ipv4.ip_nonlocal_bind=1" | tee -a /etc/sysctl.conf

# Disable ipv6 to workaround an IPv6 bug and keepalived VIP assignment. If IPv6
# support is needed then re-enable and configure it properly. Google "IPv6
# tentative dadfailed" for more info.
echo "net.ipv6.conf.${NIC}.disable_ipv6=1" | tee -a /etc/sysctl.conf

# Open some ports.
firewall-cmd --add-port=${HAPROXY_PORT_RANGE}/tcp --permanent
firewall-cmd --add-port=${IGUANA_DASHBOARD}/tcp --permanent
firewall-cmd --add-port=${IGUANA_HTTPS}/tcp --permanent
firewall-cmd --direct --permanent --add-rule ipv4 filter INPUT 0 --in-interface eth0 --destination 224.0.0.18 --protocol vrrp -j ACCEPT
firewall-cmd --direct --permanent --add-rule ipv4 filter OUTPUT 0 --out-interface eth0 --destination 224.0.0.18 --protocol vrrp -j ACCEPT

# Install HAProxy
yum install -y haproxy

# Configure HAProxy 
sed -i.orig \
    -e "s/{{WK1_IP}}/${WK1_IP}/" \
    -e "s/{{WK2_IP}}/${WK2_IP}/" \
    -e "s/{{IGUANA_HTTPS}}/${IGUANA_HTTPS}/"  \
    -e "s/{{IGUANA_DASHBOARD}}/${IGUANA_DASHBOARD}/"  \
    -e "s/{{HAPROXY_PORT_RANGE}}/${HAPROXY_PORT_RANGE}/" haproxy.cfg
mv haproxy.cfg /etc/haproxy
systemctl enable haproxy

# Install Keepalived
yum install -y keepalived

# Configure Keepalived. NOTE VIP uses alternate delimeters because the value
# contains a forward slash.
sed -i.orig \
    -e "s/{{HOSTNAME}}/${HOSTNAME}/" \
    -e "s/{{NIC}}/${NIC}/" \
    -e "s/{{KEEPALIVED_PRIORITY}}/${KEEPALIVED_PRIORITY}/" \
    -e "s|{{VIP}}|${VIP}|" keepalived.conf
mv keepalived.conf /etc/keepalived/keepalived.conf
systemctl enable keepalived

chown root:root haproxy-healthcheck.bash clean-connections.bash
chmod 500 haproxy-healthcheck.bash clean-connections.bash
mv haproxy-healthcheck.bash clean-connections.bash /usr/local/bin
touch /var/log/keepalived-transitions.log

# Reboot the system for changes to take effect.
systemctl reboot
