HA_DEPLOY_RELEASE=../ha-deploy-release

LB_FILES=\
lb-bootstrap.bash\
lb-config.bash

WK_FILES=\
wk-bootstrap.bash\
wk-config.json

.PHONY: lb-init wk-init

lb-init:
	git -C $(HA_DEPLOY_RELEASE) checkout lb-init
	rm -f $(HA_DEPLOY_RELEASE)/*
	cp $(LB_FILES) $(HA_DEPLOY_RELEASE)/
	tar czf ${@}.tar.gz lb-init/
	mv ${@}.tar.gz $(HA_DEPLOY_RELEASE)/
	git -C $(HA_DEPLOY_RELEASE) add ${@}.tar.gz $(LB_FILES)
	git -C $(HA_DEPLOY_RELEASE) commit -m "lb-init Build Time: $(shell date)"

wk-init:
	git -C $(HA_DEPLOY_RELEASE) checkout wk-init
	rm -f $(HA_DEPLOY_RELEASE)/*
	cp $(WK_FILES) $(HA_DEPLOY_RELEASE)/
	tar czf ${@}.tar.gz wk-init/
	mv ${@}.tar.gz $(HA_DEPLOY_RELEASE)/
	git -C $(HA_DEPLOY_RELEASE) add ${@}.tar.gz $(WK_FILES)
	git -C $(HA_DEPLOY_RELEASE) commit -m "wk-init Build Time: $(shell date)"

